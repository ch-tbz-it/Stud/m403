![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../../x_gitressourcen/M403_Picto.jpg)

[TOC]


# M403 - Windows PowerShell (WPS) lernen mit dem Miroboard (SOL)

---

## Unterlagen Teil 1 WPS

(Im Modul 404 wird Teil 2 Thema sein. Im Modul 122 dann Teil 3.)

1. **Unterlagen**: [WPS\_Teil\_1 Prozedural.pdf](./WPS_Teil_1 Prozedural.pdf)
2. **Kompetenzraster** und **Checkpoints**: [LB Kompetenzraster 403.pdf](LB Kompetenzraster 403.pdf)
3. WPS **Scripte** zu den Unterlagen: [08 Scripte WPS\_Teil\_1.zip](./08\ Scripte\ WPS_Teil_1.zip) herunterladen! *
4. WPS **Scripte** zu den Demos: [08 Scripte WPS\_Teil\_1 LP Demos.zip](./08\ Scripte\ WPS_Teil_1\ LP\ Demos.zip) herunterladen! *

*) --> Entzippen in den eigenen M403-Arbeits-Ordner!

## Lehrvideos ![Symbol](../../x_gitressourcen/Video.png)

[MS Stream Kanal M403 Windows PowerShell](https://web.microsoftstream.com/channel/21ba09d5-1dca-4ab9-9f0e-777d7b0c28b1)

> **Hinweis:** Erwähnungen und Referenzen in den Screencasts zum sog. "**BSCW**" sind nun auf **GitLAB**!


## Miroboard mit Gruppenavataren

> Siehe Link im Teams!

![M403 Miroboard](../../x_gitressourcen/M403_Miro.png)

