![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../../x_gitressourcen/m403_picto.jpg)

[TOC]

# M403 - Struktogramme erstellen und lesen (S1 - S4)

---

## Wozu braucht man ein Struktogramm?


Ein Struktogramm ist eine grafische Notation für eine Algorithmusbeschreibung. Struktogramme können sowohl für einfache als auch für komplexe Algorithmen verwendet werden.


# Tool um Struktogramme zu erstellen

- [Download **Structorizer**](https://structorizer.fisch.lu/index.php?include=downloads)
- [Evtl. download **Java 17** ( > Version 11 )](https://www.oracle.com/java/technologies/downloads/) ([Direct Link to download win exe](https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe))


- Folgende **Einstellung** sind vorab zu machen <br> ![Einstellungen](Structrizer Einstellungen.png)
- [**User Guide** Structorizer](https://help.structorizer.fisch.lu/)


# Unterlagen Struktogramm

- [**Skript Struktogramme**](./Skript\ Struktogramm.pdf)
- [Beispiele](./Beispiele)
- [Übungen](./Übungen)
- [Lösungen zu den Übungen](./Lösungen)
 

## Plakat Strukturen

![Plakat](./ProgBASICS\ Strukturen\ V1.3\ 150dpi.jpg)
[PDF Version](./ProgBASICS\ Strukturen\ V1.3.graffle.pdf)



