![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/M403_Picto.jpg)

[TOC]


# M403 - Windows PowerShell lernen mit dem Miroboard

---

## Themen Ordner

1. [**Unterlagen** Windows PowerShell (Alle ausser S)](./1_Unterlagen_WPS )
2. [**Übungen** zu Variablen und Berechnungen (**D1, I1**)](./2_Variablen_und_Berechnungen)
3. [**Struktogramm** kennelernen (**S1 - S4**)](./3_Struktogramm)
4. [**Testen** (**T2 - T4**)](./4_Testen)
5. [**Aufgabenstellungen** (**I3, I4**)](./5_Aufgabenstellungen)
6. [Weitere Ressourcen zu Windows POwerShell](./6_Weitere_Ressourcen)

Ressourcen für die Kompetenznachweise ( >> Siehe Link im M403-Miroboard