![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m403 Picto](../../x_gitressourcen/M403_Picto.jpg)

[TOC]


# M403 - Variablen und Berechnungen (D1, I1)

---

## Plakat Variablen

![Plakat](./ProgBASICS\ Variablen\ V1.2\ 150dpi.jpg)

**In diesem Ordner befinden sich die Scripte der folgenden Video Demos:**

> Die Scripte herunterladen und die Endung ".txt" entfernen, um sie im Editor laufen lassen zu können!

### Ordner:

1. [Übungen](./Übungen)
2. [Lösungen der Übungen](./Lösungen)


## Variablen sind "Behälter" (D1)
![Video:](./x_gitressourcen/Video.png)
[![Einführung](./x_gitressourcen/VidCapD1a.png)](https://web.microsoftstream.com/video/ad3b8697-6913-4276-8396-11a413c3406b?channelId=21ba09d5-1dca-4ab9-9f0e-777d7b0c28b1)

## Variablen mit WPS (D1)

![Video:](./x_gitressourcen/Video.png)
[![Variablen mit WPS](./x_gitressourcen/VidCapD1b.png)](https://web.microsoftstream.com/video/91a6a3e7-6277-460b-a9a0-4b6ad733d1a0?channelId=21ba09d5-1dca-4ab9-9f0e-777d7b0c28b1)



<br>
<br>
<br>

---

![Typenkonversion](./Typenkonversion.png)