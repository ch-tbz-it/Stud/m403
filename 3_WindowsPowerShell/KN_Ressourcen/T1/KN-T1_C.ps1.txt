﻿$ein = read-host "Zahl zwischen 3 und 6 ?"
if (($ein -ge 3) -and ($ein -le 6)) {
   for ( [int]$hor = 1; $hor -le $ein; $hor++) {
       [string] $out = ""
       for ( [int]$vert = $hor; $vert -le $ein; $vert++) {
           $out = $out + "*"
       }
       write-host $out
   }
} else {
   write-host Falsche Eingabe!
}
write-host "Und fertig!"