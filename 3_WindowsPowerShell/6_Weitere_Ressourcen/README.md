![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../../x_gitressourcen/M403_Picto.jpg)

[TOC]


# M403 - Windows PowerShell Ressourcen

---

1. MS Benutzerhandbuch ([pdf](./Benutzerhandbuch.pdf))
2. MS Laarn: [Was ist PowerShell](https://learn.microsoft.com/de-de/powershell/scripting/overview?redirectedfrom=MSDN&view=powershell-7.3)
2. www.msXfaq.de: [PowerShell](https://www.msxfaq.de/code/powershell/index.htm)
3. [Referenzen und Cheat-Sheets](./Referenzen)
