![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/M403_Picto.jpg)

[TOC]


# M403 - Windows PowerShell lernen mit dem Miroboard

---

## Was ist SOL? Erklärung und Begriffe

[Lernenden-zentriertes Lernen: SOL](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-SOL)

## SOL Regeln

[Regeln](https://gitlab.com/ch-tbz-it/Stud/m319/-/blob/main/N0-SOL/SOL_Learning_Rules.md)

## Kompetenzraster und Checkpoints

[PDF Version](../3_WindowsPowerShell/1_Unterlagen_WPS /LB Kompetenzraster 403.pdf)

Auszug:

# Konzept für den Kompetenznachweis des Moduls 403
Als Grundlage für diesen Kompetenznachweis dient das Kompetenzraster. Das Ziel ist es, dass die Lernenden die aufgeführte Kompetenz für die verschiede-nen Handlungsziele ausweisen können. Die Komplexität der beschriebenen Kompetenzen erhöht sich in ihrer Vollständigkeit und Komplexität von Spalte 1 zur Spalte 4. Die Spalten 3 und 4 müssen in jeweils voneinander unabhängigen (Teil-)Projekten nachgewiesen werden.

## Nachweis einer Kompetenz:	
Der einzelne Lernende hat die beschriebenen Kompetenzen im Verlaufe des Moduls nachzuweisen. Ist die entsprechende Kompetenz nachgewiesen worden, so wird dies durch das Visum des Coachs und des Lernenden im dazugehörigen Feld bestätigt. 
Bewertet wird grundsätzlich nur die vom Lernenden erbrachte Eigenleistung. Fremde Quellen sind zu kennzeichnen und zu kommentieren.

## Notengebung:	
Für eine Note 4 (bestanden) sind alle Handlungsziele der Spalten A und B nachzuweisen. Nachdem die Note 4 sichergestellt ist, kann mit dem Nachweis von zusätzlichen Kompetenzen entsprechend eine höhere Note erreicht werden.
Fehlen in einer Zeile alle Kompetenzen, wird ein Abzug von -0.75 berechnet.
<br>

Folgende Formen sind für den Nachweis einer Kompetenz denkbar, bzw. gefordert:

- Multiple-Choice-Nachweis (ROT Einzelbewertung)
Die rot markierten Kompetenzfelder (S1, S2, D1, D2, T2) müssen mit einem MC-Nachweis abgelegt werden. Erlaubte Unterlagen: eigenes ePortfolio.
- Auftrags (BLAU Tandembewertung)
Die blau markierten Kompetenzfelder (I1, I2, S3, D3, T1, T3) müssen mit einem Auftrag abgelegt werden. Erlaubte Unterlagen: eigenes ePortfolio.
- Konkretes Produkt, konkrete Beispiele (SCHWARZ Einzelbewertung)
Der Lernende zeigt und kommentiert die Kompetenz in Form eines entsprechenden Produkts (Programm, Dokumentation, erstelltes Diagramm, usw.)
- Bericht (SCHWARZ Einzelbewertung)
Der Lernende hält seine Erkenntnisse in einem selbst erstellten Bericht fest und kommentiert es anhand eines Fachgespräches. 
- Sonstiges
Es sind auch andere Formen denkbar. Im Fokus steht immer der Nachweis der entsprechenden Teilkompetenz anhand von Eigenleistungen.

Der persönliche Lernfortschritt und die eingebrachten Nachweise sind ...
- in einem **ePortfolio** zu sammeln, zu strukturieren und zugänglich zu machen (Link im Miroboard). Das ePortfolio darf für die MC-Nachweise und die Aufträge verwendet werden.
- im Kompetenzjournal (KANBAN Lernplaner im Miroboard) festzuhalten. 
- vorausschauend zu planen mit einem geeigneten Planungswerkzeug (siehe Zeitbedarf).

> **Ohne diese Protokollierung kann ein Nachweis nicht gewährt werden.**
