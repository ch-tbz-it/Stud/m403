

# M403 - Einführung mit Hamster-Scratch



## Einführungsvideo
![Video:](../../x_gitressourcen/Video.png)
[![Hamster 1 Einführung](./x_gitressourcen/VidCap1.png)](https://web.microsoftstream.com/video/ce7019c5-6974-451c-afe0-2f9eb76d766c?channelId=21ba09d5-1dca-4ab9-9f0e-777d7b0c28b1)

## Ein Algorithmus entwickeln
![Video:](../../x_gitressourcen/Video.png)
[![Hamster 2 Algorithmus entwickeln ](./x_gitressourcen/VidCap2.png)](https://web.microsoftstream.com/video/ad3b8697-6913-4276-8396-11a413c3406b?channelId=21ba09d5-1dca-4ab9-9f0e-777d7b0c28b1)
